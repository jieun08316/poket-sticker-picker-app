'use client'
import {useSelector, useDispatch} from 'react-redux'
import {picker} from "@/lib/features/sticker/stickerSlice";

export default function Home() {
    const {stickers, selected} = useSelector(state => state.sticker)
    const dispatch = useDispatch()
    const handleOpenBbang = () => {
        dispatch(picker())
    }
    return (
        <main>
            <div className="ardr-button-style">
                <button onClick={handleOpenBbang}>빵먹기</button>

            </div>

            {selected && <div className=" ardr-font-style-s">
                지금 얻은 스티커는? :  &nbsp;&nbsp;  <img src={`/photo/${selected}`} style={{width: 150, height : 200}}/>
            </div>}
            <div className="ardr-font-style">아래 도감을 모아보세요!</div>

            <div>
                <ul>

                    {stickers.map((sticker, index) => (

                        <li key={index}>{sticker.isPick ?
                            <img src={`/photo/${sticker.imgName}`} style={{width: 150}}/> : '?'}</li>

                    ))}

                </ul>
            </div>
        </main>
    );
}
