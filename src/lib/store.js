import {configureStore} from '@reduxjs/toolkit'
import StickerReducer from "@/lib/features/sticker/stickerSlice";
//https://redux-toolkit.js.org/usage/nextjs <-차례대로 만들기 store.js
export default configureStore({
    reducer: {
        sticker: StickerReducer
    },
});


